#language:ru
  @tag

Функционал: Главная страница игры 2048

  Сценарий: Проверка функционала кнопки Заново

    И открыть страницу с игрой
    И нажать клавишу "вправо"
    И проверить что количество квадратов с чилами в таблице равно 3
    И начать игру заново
    И проверить что количество квадратов с чилами в таблице равно 2

package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {
    private static final String CONFIG_FILE_PATH = "src/main/resources/config.properties";

    public static Properties readProperties() throws IOException {
        FileInputStream fileInputStream = new FileInputStream(PropertiesReader.CONFIG_FILE_PATH);
        Properties prop = new Properties();
        try {
            prop.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fileInputStream.close();
        }
        return prop;
    }
}
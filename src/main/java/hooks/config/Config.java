package hooks.config;

import com.codeborne.selenide.Selenide;
import utils.PropertiesReader;
import io.cucumber.java.After;
import io.cucumber.java.BeforeAll;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.remote.CapabilityType;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Configuration.browserCapabilities;

public class Config {
    @BeforeAll
    public static void setDriverConfig() throws IOException {
        remote = PropertiesReader.readProperties().getProperty("remoteURL");
        timeout = Long.parseLong(PropertiesReader.readProperties().getProperty("timeout"));
        browser = PropertiesReader.readProperties().getProperty("browser");
        browserVersion = PropertiesReader.readProperties().getProperty("version");
        MutableCapabilities cap = new MutableCapabilities();
        Map<String, Object> map = new HashMap<>();
        map.put("enableVNC", true);
        map.put("enableVideo", true);
        cap.setCapability("selenoid:options", map);
        cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        browserCapabilities = cap;
    }

    @After
    public static void closeDriver() {
        Selenide.closeWebDriver();
    }
}

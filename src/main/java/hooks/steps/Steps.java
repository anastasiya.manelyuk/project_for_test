package hooks.steps;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Selenide;
import io.cucumber.java.ru.И;
import org.openqa.selenium.Keys;
import pages.MainPage;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;

public class Steps {

    MainPage mainPage = new MainPage();

    @И("открыть страницу с игрой")
    public void openGame(){
        open("https://eager-bassi-4a9fdf.netlify.app/");
        //sleep(5);
    }

    @И("проверить что количество клеток в таблице равно {int}")
    public void checkGridCell(int amount){
       mainPage.getAllGridCell().shouldBe(CollectionCondition.size(amount));
    }
    @И("проверить что количество квадратов с чилами в таблице равно {int}")
    public void checkTile(int amount){
        mainPage.getAllTile().shouldBe(CollectionCondition.size(amount));
    }

    @И("начать игру заново")
    public void replayGame(){
        mainPage.getRelayButton().click();
    }

    @И("нажать клавишу {string}")
    public void pressButton(String condition){
        switch (condition){
            case "вниз":
            Selenide.actions()
                    .sendKeys(Keys.DOWN).perform();
            case "вверх":
                Selenide.actions()
                        .sendKeys(Keys.UP).perform();
            case "влево":
                Selenide.actions()
                        .sendKeys(Keys.LEFT).perform();
            case "вправо":
                Selenide.actions()
                        .sendKeys(Keys.RIGHT).perform();
        }
    }
}

package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage {
    private String tile = "//div[contains(@class, 'tile') and text()]";
    private String gridCell = "//div[contains(@class, 'grid-cell')]";
    private String buttonReplay = "//button[text()='Заново']";

    public SelenideElement getRelayButton(){
        return $x(buttonReplay);
    }

    public ElementsCollection getAllGridCell(){
        return $$x(gridCell);
    }

    public ElementsCollection getAllTile(){
        return $$x(tile);
    }
}
